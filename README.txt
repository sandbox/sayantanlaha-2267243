
Installation

1. Place the track_visitor_ip folder in the custom/contrib modules directory of your 
Drupal installation.

2. Enable the module at admin/modules.

Operation

This module displays your site visitors IP Address, count of visit & visiting time. Administrator can check the details of visitors 
from admin/config/system/track_visitor_ip section. later on we will put some options, so administrator can block any IP if required.